package com.creativemindsz.poseidon.CoreModules;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.creativemindsz.poseidon.OtherUtils.CustomIntent;
import com.creativemindsz.poseidon.OtherUtils.CustomPoseidonDialog;
import com.creativemindsz.poseidon.OtherUtils.LabeledSwitch;
import com.creativemindsz.poseidon.OtherUtils.LocalSharedPref;
import com.creativemindsz.poseidon.R;

import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class Constants {


    private static Constants baseConfig = null;

    public static boolean server_connectivity_status = false;
    public static String AppNodeIP="";
    public static String AppDBFolderName="";

    public static String Appname = "";
    public static String AppLogo = "";
    public static String AppLanguage = "";
    public static String AppWebServiceType = "";
    public static String AppImagephpURL = "";
    public static String AppDatabaseName = "";
    public static String AppDirectoryPictures = Appname;


    public Constants getInstance() {
        if (baseConfig == null) {
            baseConfig = new Constants();
            return baseConfig;
        }
        return baseConfig;
    }

    public static void LoadAppConfiguration(Context ctx) {

        Appname = new LocalSharedPref(ctx).getValue("Appname");
        AppLanguage = new LocalSharedPref(ctx).getValue("AppLanguage");
        AppWebServiceType = new LocalSharedPref(ctx).getValue("AppWebServiceType");
        AppNodeIP = new LocalSharedPref(ctx).getValue("AppNodeIP");
        AppImagephpURL = new LocalSharedPref(ctx).getValue("AppImagephpURL");
        AppDBFolderName = new LocalSharedPref(ctx).getValue("AppDBFolderName");
        AppDatabaseName = new LocalSharedPref(ctx).getValue("AppDatabaseName");
        AppDirectoryPictures = new LocalSharedPref(ctx).getValue("AppDirectoryPictures");
        AppLogo = new LocalSharedPref(ctx).getValue("AppLogo");

    }


    public static void SnackBar(Context ctx, String Message, View parentLayout, int id) {

        Snackbar mSnackBar = Snackbar.make(parentLayout, Message, Snackbar.LENGTH_LONG);
        View view = mSnackBar.getView();
        view.setPadding(5, 5, 5, 5);

        if (id == 1)//Positive
        {
            view.setBackgroundColor(ctx.getResources().getColor(R.color.colorPrimary));
        } else if (id == 2)//Negative
        {
            view.setBackgroundColor(ctx.getResources().getColor(R.color.orange_300));
        } else//Negative
        {
            view.setBackgroundColor(ctx.getResources().getColor(R.color.red_400));
        }


        TextView mainTextView = (view).findViewById(android.support.design.R.id.snackbar_text);
        mainTextView.setAllCaps(true);
        mainTextView.setTextSize(16);
        mainTextView.setTextColor(ctx.getResources().getColor(R.color.white));
        mSnackBar.setDuration(4000);
        mSnackBar.show();


    }

    public static void ExitSweetDialog(final Context ctx, final Class<?> className) {

        new CustomPoseidonDialog(ctx)
                .setLayoutColor(R.color.green_500)
                .setImage(R.drawable.ic_exit_to_app_black_24dp)
                .setTitle(ctx.getResources().getString(R.string.message_title))
                .setDescription(ctx.getResources().getString(R.string.are_you_sure_want_to_exit))
                .setPossitiveButtonTitle("YES")
                .setNegativeButtonTitle("NO")
                .setOnPossitiveListener(((Activity) ctx)::finishAffinity);


    }

    public static void globalStartIntent(Context context,Class classes,Bundle bundle)
    {
        ((Activity)context).finish();
        Intent intent=new Intent(context,classes);
        if(bundle!=null)
        {
            intent.putExtras(bundle);
        }
        CustomIntent.customType(context, 4);
        context.startActivity(intent);

    }

    public static void globalStartIntent2(Context context,Class classes,Bundle bundle)
    {
        ((Activity)context).finish();
        Intent intent=new Intent(context,classes);
        if(bundle!=null)
        {
            intent.putExtras(bundle);
        }
        CustomIntent.customType(context, 1);
        context.startActivity(intent);

    }

    public static void checkNodeServer(ImageView imgview) throws InterruptedException {

        new Thread() {

            Handler messageHandler = new Handler() {
                public void handleMessage(Message msg) {

                    super.handleMessage(msg);
                    if (server_connectivity_status) {
                        server_connectivity_status = true;
                        if (imgview != null) {
                            imgview.setImageResource(R.drawable.ic_status_ready);
                        }
                    } else {
                        server_connectivity_status = false;

                        if (imgview != null) {
                            imgview.setImageResource(R.drawable.ic_status_busy);
                        }
                    }
                }
            };

            public void run() {

                CheckServerConnection();
                messageHandler.sendEmptyMessage(0);
            }


        }.start();


    }

    public static void CheckServerConnection() {
        try {
            HttpURLConnection conn = (HttpURLConnection) (new URL(Constants.AppNodeIP)).openConnection();
            conn.setUseCaches(false);
            conn.connect();
            int status = conn.getResponseCode();

            if (status == 200) {
                server_connectivity_status = true;
            } else {
                server_connectivity_status = false;
            }

            conn.disconnect();

        } catch (Exception e) {
            Log.e("doHttpGetRequest", e.toString());
        }
    }

    public static boolean CheckNetwork(Context ctx) {
        ConnectivityManager cn = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        if (nf != null && nf.isConnected() == true) {

            return true;
        } else {

            return false;
        }
    }


    public static String DeviceDate() {
        String date = "";

        //2017/04/09 19:51:10
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        SimpleDateFormat dateformt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        date = dateformt.format(Date.parse(currentDateTimeString));

        return date;
    }

    public static String saveURLImagetoSDcard(Bitmap bitmap, String imagename) {
        String mBaseFolderPath = Constants.AppDBFolderName + "/_Photos/";

        if (!new File(mBaseFolderPath).exists()) {
            new File(mBaseFolderPath).mkdir();
        }

        String mFilePath = mBaseFolderPath + imagename + ".png";


        File mypath = new File(mFilePath);

        if (mypath.exists()) {
            mypath.delete();
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mFilePath;

    }

    public static void Glide_LoadImageView(ImageView imgvw, String location) {
        try {

            Glide.with(imgvw.getContext()).load(new File(location)).into(imgvw);//.onLoadFailed(imgvw.getResources().getDrawable(R.drawable.no_media));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void Glide_LoadDefaultImageView(ImageView imgvw) {
        Glide.with(imgvw.getContext()).load(Uri.parse("file:///android_asset/image.png")).into(imgvw);//.onLoadFailed(imgvw.getResources().getDrawable(R.drawable.no_media));
    }

    public static Bitmap Glide_GetBitmap(Context ctx, String path) {
        try {
            Bitmap ret_bitmap = null;

            ret_bitmap = Glide.with(ctx).asBitmap().load(path).into(150, 150).get();

            return ret_bitmap;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void HideKeyboard(Context ctx) {
        if (((Activity) ctx).getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) ((Activity) ctx).getSystemService(INPUT_METHOD_SERVICE);
            ;
            inputMethodManager.hideSoftInputFromWindow(((Activity) ctx).getCurrentFocus().getWindowToken(), 0);
        }
        ((Activity) ctx).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    public static String[] convertListtoStringArray(List<String> liststring) {

        String[] ValueArr = new String[liststring.size()];
        ValueArr = liststring.toArray(ValueArr);

        return ValueArr;
    }


    /**
     * All control basic operations
     * Muthukumar N
     * 03-05-2018
     */
    public static String GetWidgetOperations(View control, int id) {
        // android.support.v7.widget.AppCompatAutoCompleteTextView
        // 05-03 19:44:20.435 17525-17525/displ.mobydocmarathi.com E/check 2:: class android.support.v7.widget.AppCompatAutoCompleteTextView
        //Log.e("check 1: ",control.getClass().getName().toString());
        // Log.e("check 2: ",control.getClass().toString());
        String str = "";

        if (control instanceof EditText) {

            EditText edt = (EditText) control;
            str = EditTextOperations(edt, id, "", "");

        } else if (control instanceof TextView) {
            TextView txtvw = (TextView) control;
            str = TextViewOperations(txtvw, id, "", "");

        } else if (control instanceof AutoCompleteTextView) {
            AutoCompleteTextView edt = (AutoCompleteTextView) control;
            str = AutoCompleteTextViewOperations(edt, id, "", "");

        } else if (control instanceof MultiAutoCompleteTextView) {
            MultiAutoCompleteTextView edt = (MultiAutoCompleteTextView) control;
            str = MultiAutoCompleteTextViewOperations(edt, id, "", "");

        } else if (control instanceof ToggleButton) {
            ToggleButton tbg = (ToggleButton) control;
            str = ToggleButtonOperations(tbg, id, "");

        } else if (control instanceof LabeledSwitch) {
            LabeledSwitch tbg = (LabeledSwitch) control;
            str = LabeledSwitchOperations(tbg, id, "");

        } else if (control instanceof CheckBox) {
            CheckBox chk = (CheckBox) control;
            str = CheckBoxButtonOperations(chk, id, "");

        } else if (control instanceof RadioButton) {
            RadioButton rbtn = (RadioButton) control;
            str = RadioButtonOperations(rbtn, id, "");

        } else if (control instanceof RadioGroup) {
            RadioGroup rgrp = (RadioGroup) control;
            str = RadioGroupOperations(rgrp, id, "");

        } else if (control instanceof Switch) {
            Switch swt = (Switch) control;
            str = SwitchButtonOperations(swt, id, "");

        } else if (control instanceof Spinner) {
            Spinner spn = (Spinner) control;

            str = SpinnerOperations(spn, id);


        }

        return str;

    }

    /**
     * All control basic operations - EDIT_TEXT
     * Muthukumar N
     * 04-05-2018
     */
    public static String EditTextOperations(EditText edt, int id, String setdata, String errormsg) {
        String str = "";
        switch (id) {
            case 1://Get text
                str = edt.getText().toString();
                break;

            case 2://Set text
                edt.setText(setdata);
                break;

            case 3://set enable - true
                edt.setEnabled(true);
                break;

            case 4://set enable - false
                edt.setEnabled(false);
                break;

            case 5://get text length
                if (edt.getText().length() > 0) {
                    str = "true";
                } else {
                    str = "false";
                }
                break;

            case 6://set error
                edt.setError(errormsg);
                break;
        }


        return str;

    }

    public static String TextViewOperations(TextView edt, int id, String setdata, String errormsg) {
        String str = "";
        switch (id) {
            case 1://Get text
                str = edt.getText().toString();
                break;

            case 2://Set text
                edt.setText(setdata);
                break;

            case 3://set enable - true
                edt.setEnabled(true);
                break;

            case 4://set enable - false
                edt.setEnabled(false);
                break;

            case 5://get text length
                if (edt.getText().length() > 0) {
                    str = "true";
                } else {
                    str = "false";
                }
                break;

            case 6://set error
                edt.setError(errormsg);
                break;
        }


        return str;

    }

    public static String AutoCompleteTextViewOperations(AutoCompleteTextView edt, int id, String setdata, String errormsg) {
        String str = "";
        switch (id) {
            case 1://Get text
                str = edt.getText().toString();
                break;

            case 2://Set text
                edt.setText(setdata);
                break;

            case 3://set enable - true
                edt.setEnabled(true);
                break;

            case 4://set enable - false
                edt.setEnabled(false);
                break;

            case 5://get text length
                if (edt.getText().length() > 0) {
                    str = "true";
                } else {
                    str = "false";
                }
                break;

            case 6://set error
                edt.setError(errormsg);
                break;

            case 7:
                edt.setThreshold(1);
                break;

        }


        return str;

    }

    public static String MultiAutoCompleteTextViewOperations(MultiAutoCompleteTextView edt, int id, String setdata, String errormsg) {
        String str = "";
        switch (id) {
            case 1://Get text
                str = edt.getText().toString();
                break;

            case 2://Set text
                edt.setText(setdata);
                break;

            case 3://set enable - true
                edt.setEnabled(true);
                break;

            case 4://set enable - false
                edt.setEnabled(false);
                break;

            case 5://get text length
                if (edt.getText().length() > 0) {
                    str = "true";
                } else {
                    str = "false";
                }
                break;

            case 6://set error
                edt.setError(errormsg);
                break;

            case 7:
                edt.setThreshold(1);
                break;

        }
        return str;
    }

    public static String LabeledSwitchOperations(LabeledSwitch toggleButton, int id, String setdata) {
        String str = "";
        switch (id) {

            case 1:
                if (toggleButton.isOn()) {
                    str = toggleButton.getLabelOn();
                } else {
                    str = toggleButton.getLabelOff();
                }
                break;
        }

        return str;
    }

    public static String ToggleButtonOperations(ToggleButton toggleButton, int id, String setdata) {
        String str = "";
        switch (id) {
            case 1://get text
                str = toggleButton.getTextOn().toString();
                break;

            case 2://set on text
                toggleButton.setTextOn(setdata);
                break;

            case 3://set off text
                toggleButton.setTextOff(setdata);
                break;

            case 4:
                if (toggleButton.isChecked()) {
                    str = "true";
                } else {
                    str = "false";
                }
                break;
        }

        return str;
    }

    public static String SpinnerOperations(Spinner spn, int id) {
        String str = "";
        switch (id) {
            case 1:
                if (spn.getSelectedItemPosition() > 0) {
                    str = spn.getSelectedItem().toString();
                }
                break;


        }

        return str;

    }

    public static String SwitchButtonOperations(Switch toggleButton, int id, String setdata) {
        String str = "";
        switch (id) {
            case 1://get text
                str = toggleButton.getTextOn().toString();
                break;

            case 2://set on text
                toggleButton.setTextOn(setdata);
                break;

            case 3://set off text
                toggleButton.setTextOff(setdata);
                break;

            case 4:
                if (toggleButton.isChecked()) {
                    str = "true";
                } else {
                    str = "false";
                }
                break;
        }

        return str;
    }
    // Common methods to get all control values

    public static String RadioGroupOperations(RadioGroup rdgrp, int id, String setdata) {
        String str = "";
        switch (id) {
            case 1:
                // Get the checked Radio Button ID from Radio Group
                int selectedRadioButtonID = rdgrp.getCheckedRadioButtonId();

                // If nothing is selected from Radio Group, then it return -1
                if (selectedRadioButtonID != -1) {
                    RadioButton selectedRadioButton = (RadioButton) ((Activity) rdgrp.getContext()).findViewById(selectedRadioButtonID);
                    String selectedRadioButtonText = selectedRadioButton.getText().toString();
                    str = selectedRadioButtonText;
                } else {
                    str = "-";
                }
                break;

            case 2:
                // Get the checked Radio Button ID from Radio Group
                selectedRadioButtonID = rdgrp.getCheckedRadioButtonId();

                // If nothing is selected from Radio Group, then it return -1
                if (selectedRadioButtonID == -1) {
                    str = "false";
                } else {
                    str = "true";
                }

                break;

        }

        return str;
    }

    public static String RadioButtonOperations(RadioButton rbtn, int id, String setdata) {
        String str = "";
        switch (id) {
            case 1:
                if (rbtn.isChecked()) {
                    str = rbtn.getText().toString();
                }

                break;

            case 2:
                rbtn.setText(setdata);
                break;

            case 3:
                if (rbtn.isChecked()) {
                    str = "true";
                } else {
                    str = "false";
                }
                break;
        }

        return str;
    }

    public static String CheckBoxButtonOperations(CheckBox rbtn, int id, String setdata) {
        String str = "";
        switch (id) {
            case 1:
                if (rbtn.isChecked()) {
                    str = rbtn.getText().toString();
                }

                break;

            case 2:
                rbtn.setText(setdata);
                break;

            case 3:
                if (rbtn.isChecked()) {
                    str = "true";
                } else {
                    str = "false";
                }
                break;
        }

        return str;
    }





}//END
