package com.creativemindsz.poseidon.CoreModules.Products;

import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.creativemindsz.poseidon.Adapters.CommonObjects;
import com.creativemindsz.poseidon.CoreModules.Constants;
import com.creativemindsz.poseidon.CoreModules.Dashboard;
import com.creativemindsz.poseidon.R;

import java.util.ArrayList;
import java.util.List;


public class Products extends AppCompatActivity{


    private ViewPager view_pager;
    private SectionsPagerAdapter viewPagerAdapter;
    private TabLayout tab_layout;
    FragmentTabsProducts fragmentTabsProducts=null;
    private SearchView searchView;
    private  FragmentTabsKeywords fragmentTabsKeywords;
    private FragmentTabsKeywordsxApplication fragmentTabsKeywordsxApplication;
    List<String> titles=new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poseidon_products_info);

        titles.add("Products Info - List");
        titles.add("Products - Keywords");
        titles.add("Products - KeywordxApplication");

        initToolbar();
        initComponent();

    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Products Info");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void initComponent() {
        view_pager = findViewById(R.id.view_pager);
        tab_layout = findViewById(R.id.tab_layout);
        searchView=findViewById(R.id.searchView);


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public boolean onQueryTextSubmit(String query) {
                callSearch(query);
                return true;
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public boolean onQueryTextChange(String newText) {
//              if (searchView.isExpanded() && TextUtils.isEmpty(newText)) {
                callSearch(newText);
//              }
                return true;
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            public void callSearch(String query) {

                int valuesposition=tab_layout.getSelectedTabPosition();

                if (valuesposition==0) {
                    fragmentTabsProducts.filterList(query);
                }else if (valuesposition==1)
                {
                    fragmentTabsKeywords.filterList(query);
                }else if (valuesposition==2)
                {
                    fragmentTabsKeywordsxApplication.filterList(query);
                }


            }

        });


        setupViewPager(view_pager);
        tab_layout.setupWithViewPager(view_pager);

        tab_layout.getTabAt(0).setIcon(R.drawable.ic_vector_dashboard_menu2_products);
        tab_layout.getTabAt(1).setIcon(R.drawable.ic_tabs_keywords);
        tab_layout.getTabAt(2).setIcon(R.drawable.ic_book);

        // set icon color pre-selected
        tab_layout.getTabAt(0).getIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
        tab_layout.getTabAt(1).getIcon().setColorFilter(getResources().getColor(R.color.grey_20), PorterDuff.Mode.SRC_IN);
        tab_layout.getTabAt(2).getIcon().setColorFilter(getResources().getColor(R.color.grey_20), PorterDuff.Mode.SRC_IN);

        ((TextView)findViewById(R.id.title)).setText(titles.get(0));
        tab_layout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                getSupportActionBar().setTitle(viewPagerAdapter.getTitle(tab.getPosition()));
                tab.getIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
                ((TextView)findViewById(R.id.title)).setText(titles.get(tab.getPosition()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(getResources().getColor(R.color.grey_20), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



        findViewById(R.id.viewType).setOnClickListener(v -> {
            int valuesposition=tab_layout.getSelectedTabPosition();

            if(valuesposition==0) {
                if (fragmentTabsProducts.viewType == CommonObjects.ViewType.ListView) {
                    fragmentTabsProducts.viewType = CommonObjects.ViewType.GrideView;
                    fragmentTabsProducts.linearlayout.setVisibility(View.VISIBLE);
                    ((ImageView) findViewById(R.id.viewType)).setImageResource(R.drawable.ic_gride);
                    fragmentTabsProducts.Product_RecyclerList();
                } else {
                    fragmentTabsProducts.viewType = CommonObjects.ViewType.ListView;
                    fragmentTabsProducts.linearlayout.setVisibility(View.GONE);
                    ((ImageView) findViewById(R.id.viewType)).setImageResource(R.drawable.ic_list);
                    fragmentTabsProducts.Product_RecyclerList();
                }

            }else if(valuesposition==1)
            {
                if (fragmentTabsKeywords.viewType == CommonObjects.ViewType.ListView) {
                    fragmentTabsKeywords.viewType = CommonObjects.ViewType.GrideView;
                    fragmentTabsKeywords.linearLayout.setVisibility(View.VISIBLE);
                    ((ImageView) findViewById(R.id.viewType)).setImageResource(R.drawable.ic_gride);
                    fragmentTabsKeywords.Product_RecyclerList();
                } else {
                    fragmentTabsKeywords.viewType = CommonObjects.ViewType.ListView;
                    fragmentTabsKeywords.linearLayout.setVisibility(View.GONE);
                    ((ImageView) findViewById(R.id.viewType)).setImageResource(R.drawable.ic_list);
                    fragmentTabsKeywords.Product_RecyclerList();
                }

            }else if(valuesposition==2)
            {
                if (fragmentTabsKeywordsxApplication.viewType == CommonObjects.ViewType.ListView) {
                    fragmentTabsKeywordsxApplication.viewType = CommonObjects.ViewType.GrideView;
                    fragmentTabsKeywordsxApplication.linearLayout.setVisibility(View.VISIBLE);
                    ((ImageView) findViewById(R.id.viewType)).setImageResource(R.drawable.ic_gride);
                    fragmentTabsKeywordsxApplication.Product_RecyclerList();
                } else {
                    fragmentTabsKeywordsxApplication.viewType = CommonObjects.ViewType.ListView;
                    fragmentTabsKeywordsxApplication.linearLayout.setVisibility(View.GONE);
                    ((ImageView) findViewById(R.id.viewType)).setImageResource(R.drawable.ic_list);
                    fragmentTabsKeywordsxApplication.Product_RecyclerList();
                }

            }





        });



    }

    private void setupViewPager(ViewPager viewPager) {
        viewPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        fragmentTabsProducts=FragmentTabsProducts.newInstance();
        fragmentTabsKeywords=FragmentTabsKeywords.newInstance();
        fragmentTabsKeywordsxApplication=FragmentTabsKeywordsxApplication.newInstance();


        viewPagerAdapter.addFragment(fragmentTabsProducts, "Products Info - List");   // index 1
        viewPagerAdapter.addFragment(fragmentTabsKeywords, "Products - Keywords");    // index 0
        viewPagerAdapter.addFragment(fragmentTabsKeywordsxApplication, "Products - KeywordxApplication");
        viewPager.setAdapter(viewPagerAdapter);



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        Constants.globalStartIntent2(Products.this, Dashboard.class, null);
    }

    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);


        }

        public String getTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }
}
