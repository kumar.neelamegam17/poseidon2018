package com.creativemindsz.poseidon.CoreModules.Products;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.creativemindsz.poseidon.Adapters.CommonObjects;
import com.creativemindsz.poseidon.OtherUtils.POSEIDON_RecyclerAdapter;
import com.creativemindsz.poseidon.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentTabsKeywordsxApplication extends Fragment {


    RecyclerView recyclerViewProducts;
    List<CommonObjects.KeywordsxApplication> product_list = new ArrayList();
    List<CommonObjects.KeywordsxApplication> searchList = new ArrayList();
    CommonObjects.ViewType viewType= CommonObjects.ViewType.ListView;

    LinearLayout linearLayout;

    TextView txtvwSno,txtvwProductname,txtvwBrand,txtvwPptfiles;
    public FragmentTabsKeywordsxApplication() {
    }

    public static FragmentTabsKeywordsxApplication newInstance() {
        FragmentTabsKeywordsxApplication fragment = new FragmentTabsKeywordsxApplication();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tabs_products, container, false);

        recyclerViewProducts=root.findViewById(R.id.recycler_products);

        txtvwSno = root.findViewById(R.id.txtvw_sno);
        txtvwProductname = root.findViewById(R.id.txtvw_productname);
        txtvwBrand = root.findViewById(R.id.txtvw_brand);
        txtvwPptfiles = root.findViewById(R.id.txtvw_pptfiles);
        linearLayout = root.findViewById(R.id.linearLayout);
        txtvwProductname.setText("Keyword");
        txtvwBrand.setText("WaterApp");
        txtvwPptfiles.setText("FoodApp");


        product_list.add(new CommonObjects.KeywordsxApplication(1, "AMMONIA", "PLANKTON PLUS", "Am-Pro"));
        product_list.add(new CommonObjects.KeywordsxApplication(2, "BMMONIA", "PLANKTON PLUS", "Am-Pro"));
        product_list.add(new CommonObjects.KeywordsxApplication(3, "BMMONIA", "PLANKTON PLUS", "Am-Pro"));
        product_list.add(new CommonObjects.KeywordsxApplication(4, "CMMONIA", "PLANKTON PLUS", "Am-Pro"));
        product_list.add(new CommonObjects.KeywordsxApplication(5, "AMMONIA", "PLANKTON PLUS", "Am-Pro"));
        product_list.add(new CommonObjects.KeywordsxApplication(6, "AMMONIA", "PLANKTON PLUS", "Am-Pro"));
        product_list.add(new CommonObjects.KeywordsxApplication(7, "AMMONIA", "PLANKTON PLUS", "Am-Pro"));
        product_list.add(new CommonObjects.KeywordsxApplication(8, "AMMONIA", "PLANKTON PLUS", "Am-Pro"));
        product_list.add(new CommonObjects.KeywordsxApplication(9, "AMMONIA", "PLANKTON PLUS", "Am-Pro"));
        product_list.add(new CommonObjects.KeywordsxApplication(10, "AMMONIA", "PLANKTON PLUS", "Am-Pro"));

        Product_RecyclerList();


        return root;
    }

    public void Product_RecyclerList() {
        //Set Medical History RecylerView

        if (viewType== CommonObjects.ViewType.ListView) {
            setListView(product_list);
        }else
        {
            setGridView(product_list);
        }

    }


    private void setGridView(List<CommonObjects.KeywordsxApplication> product_lists) {
        recyclerViewProducts.setLayoutManager(new LinearLayoutManager(getActivity()));
        final POSEIDON_RecyclerAdapter KDMCRecyclerAdapter = new POSEIDON_RecyclerAdapter(product_lists, R.layout.poseidon_products_griditem)
                .setRowItemView(new POSEIDON_RecyclerAdapter.AdapterView() {
                    @Override
                    public Object setAdapterView(ViewGroup parent, int viewType, int layoutId) {
                        return new FragmentTabsProducts.ProductsViewHolder(LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false));
                    }

                    @Override
                    public void onBindView(Object holder, final int position, Object data, final List<Object> dataList) {

                        FragmentTabsProducts.ProductsViewHolder myViewHolders = (FragmentTabsProducts.ProductsViewHolder) holder;
                        final CommonObjects.KeywordsxApplication value = (CommonObjects.KeywordsxApplication) data;

                        try {
                            myViewHolders.sno.setText(String.valueOf(position + 1));
                            myViewHolders.product_name.setText(value.getKeywords());
                            myViewHolders.brand_name.setText(value.getFoodApp());
                            myViewHolders.fileppt.setText(value.getWaterApp());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                });
        recyclerViewProducts.setAdapter(KDMCRecyclerAdapter);
    }



    private void setListView(List<CommonObjects.KeywordsxApplication> product_lists) {
        recyclerViewProducts.setLayoutManager(new GridLayoutManager(getActivity(),2));
        final POSEIDON_RecyclerAdapter KDMCRecyclerAdapter = new POSEIDON_RecyclerAdapter(product_lists, R.layout.poseidon_products_rowitem)
                .setRowItemView(new POSEIDON_RecyclerAdapter.AdapterView() {
                    @Override
                    public Object setAdapterView(ViewGroup parent, int viewType, int layoutId) {
                        return new FragmentTabsProducts.ProductsViewHolder(LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false));
                    }

                    @Override
                    public void onBindView(Object holder, final int position, Object data, final List<Object> dataList) {

                        FragmentTabsProducts.ProductsViewHolder myViewHolders = (FragmentTabsProducts.ProductsViewHolder) holder;
                        final CommonObjects.KeywordsxApplication value = (CommonObjects.KeywordsxApplication) data;

                        try {
                            myViewHolders.sno.setText(String.valueOf(position + 1));
                            myViewHolders.product_name.setText(value.getKeywords());
                            myViewHolders.brand_name.setText(value.getFoodApp());
                            myViewHolders.fileppt.setText(value.getWaterApp());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                });
        recyclerViewProducts.setAdapter(KDMCRecyclerAdapter);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    void filterList(String value) {
        //Do searching
        if (!value.isEmpty()) {
    /*        List<CommonObjects.ProductInfo> result = product_list.stream()                // convert list to stream
                    .filter(line -> !line.getProductName().startsWith(value))     // we dont like mkyong
                    .collect(Collectors.toList());*/

            searchList = new ArrayList<>();
            for (CommonObjects.KeywordsxApplication productInfo : product_list) {
                if (productInfo.getKeywords().toLowerCase().startsWith(value.toLowerCase())) {
                    searchList.add(productInfo);
                }
            }

            if (viewType == CommonObjects.ViewType.ListView) {
                setListView(searchList);
            } else {
                setGridView(searchList);
            }
        }else
        {
            if (viewType == CommonObjects.ViewType.ListView) {
                setListView(product_list);
            } else {
                setGridView(product_list);
            }
        }
    }


    public static class ProductsViewHolder extends RecyclerView.ViewHolder {
        TextView keyword;
        TextView waterapp;
        TextView feedapp;
        TextView sno;

        public ProductsViewHolder(View view) {
            super(view);

            keyword = view.findViewById(R.id.txtvw_productname);
            waterapp = view.findViewById(R.id.txtvw_brand);
            feedapp = view.findViewById(R.id.txtvw_pptfiles);
            sno = view.findViewById(R.id.txtvw_sno);
        }
    }


}
