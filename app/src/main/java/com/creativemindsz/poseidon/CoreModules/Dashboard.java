package com.creativemindsz.poseidon.CoreModules;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.creativemindsz.poseidon.Adapters.GroupsGridAdapter;
import com.creativemindsz.poseidon.Adapters.MenuNavigationAdapter;
import com.creativemindsz.poseidon.AllDataObjects.Group;
import com.creativemindsz.poseidon.CoreModules.Products.Products;
import com.creativemindsz.poseidon.R;

import java.util.ArrayList;
import java.util.List;

public class Dashboard extends AppCompatActivity implements View.OnClickListener {

    RecyclerView dashboard_recyclervw;

    public GroupsGridAdapter mAdapter;

    SlidingPaneLayout mSlidingPanel;
    View bg;
    SlidingPaneLayout.PanelSlideListener panelListener = new SlidingPaneLayout.PanelSlideListener() {

        @Override
        public void onPanelClosed(View arg0) {
            // TODO Auto-genxxerated method stub

        }

        @Override
        public void onPanelOpened(View arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onPanelSlide(View arg0, float arg1) {
            // TODO Auto-generated method stub

        }

    };

    public static List<Group> getGroupData(Context ctx)  {
        List<Group> items = new ArrayList<>();

        items.add(new Group(1,"Products","List of all product info",    "ic_vector_dashboard_menu2_products"));
        items.add(new Group(2,"Farmer","List of all farmers",           "ic_vector_dashboard_menu2_farmers"));
        items.add(new Group(3,"Messages","Notifications of meetings",   "ic_vector_dashboard_menu2_messages"));

        return items;
    }



    public static int getGridSpanCount(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getMetrics(displayMetrics);
        float screenWidth  = displayMetrics.widthPixels;
        float cellWidth = activity.getResources().getDimension(R.dimen.recycler_item_size);
        return Math.round(screenWidth / cellWidth);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poseidon_dashboard);



        dashboard_recyclervw = findViewById(R.id.dashboard_recyclerview);
        LinearLayoutManager mLayoutManager = new GridLayoutManager(Dashboard.this, getGridSpanCount(Dashboard.this));
        dashboard_recyclervw.setLayoutManager(mLayoutManager);
        dashboard_recyclervw.setHasFixedSize(true);

        // specify an adapter (see also next example)
        mAdapter = new GroupsGridAdapter(dashboard_recyclervw.getContext(), getGroupData(dashboard_recyclervw.getContext()));
        dashboard_recyclervw.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener((v, obj, position) -> {

            int item_postion = obj.getId();
            switch(item_postion)
            {

                case 1:

                   // Constants.globalStartIntent(Dashboard.this, Products.class, null);
                    startActivity(new Intent(this, Products.class));

                    break;

                case 2:

                    break;

                case 3:

                    break;

            }


        });


        mSlidingPanel = findViewById(R.id.SlidingPanel);
        mSlidingPanel.setPanelSlideListener(panelListener);
        mSlidingPanel.setParallaxDistance(100);
        mSlidingPanel.setSliderFadeColor(ContextCompat.getColor(this, android.R.color.transparent));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle("Dashboard");
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_home);
        }

        bg = findViewById(R.id.imageMain);

      /*  final String url = BuildConfig.IMAGE_URL + "menu-navigation/style-6/bg-image-Menu-6-960.jpg";

        Glide.with(this)
                .load(url)
                .thumbnail(0.01f)
                .centerCrop()
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        bg.setBackground(resource);
                    }
                });*/

        final String[] mGroups = {
                "Dashboard",
                "Products",
                "Farmer",
                "Messages",
                "Profile"
        };

        final String[][] mChilds = {
                {},
                {"Keywords","Products Info","KeywordsXApplication"},
                {}, {},
                {}
        };

        final String[] mIcons = {
                "ic_vector_dashboard_side_menu1",
         "ic_vector_dashboard_menu2_products",
         "ic_vector_dashboard_menu2_farmers",
         "ic_vector_dashboard_menu2_messages",
          "ic_vector_dashboard_side_profile"

        };

        ExpandableListView listMenu = findViewById(R.id.menu_list);
        MenuNavigationAdapter adapter = new MenuNavigationAdapter(this, mGroups, mChilds, mIcons);
        listMenu.setAdapter(adapter);
        listMenu.expandGroup(1);
        listMenu.setOnChildClickListener((expandableListView, view, groupPosition, childPosition, l) -> {
            Toast.makeText(Dashboard.this, "Menu " + mChilds[groupPosition][childPosition] + " clicked!", Toast.LENGTH_SHORT).show();
            mSlidingPanel.closePane();
            return false;
        });

        listMenu.setOnGroupClickListener((expandableListView, view, groupPosition, l) -> {
            if (groupPosition > 0) {
                Toast.makeText(Dashboard.this, "Menu " + mGroups[groupPosition] + " clicked!", Toast.LENGTH_SHORT).show();
                mSlidingPanel.closePane();

                if(groupPosition==4)
                {
                    startActivity(new Intent(Dashboard.this, ProfilePage.class));
                }
            }
            return false;
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mSlidingPanel.isOpen()) {
                    mSlidingPanel.closePane();
                } else {
                    mSlidingPanel.openPane();
                }
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogout:
                Toast.makeText(this, "Button logout clicked!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnSetting:
                Toast.makeText(this, "Button setting clicked!", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }
}
