package com.creativemindsz.poseidon.CoreModules.Products;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.creativemindsz.poseidon.Adapters.CommonObjects;
import com.creativemindsz.poseidon.OtherUtils.POSEIDON_RecyclerAdapter;
import com.creativemindsz.poseidon.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentTabsKeywords extends Fragment {


    RecyclerView recyclerViewProducts;
    List<CommonObjects.Keywords> product_list = new ArrayList();
    List<CommonObjects.Keywords> searchList = new ArrayList();
    CommonObjects.ViewType viewType= CommonObjects.ViewType.ListView;
    LinearLayout linearLayout;
    TextView txtvwSno,txtvwProductname,txtvwBrand,txtvwPptfiles;
    public FragmentTabsKeywords() {
    }

    public static FragmentTabsKeywords newInstance() {
        FragmentTabsKeywords fragment = new FragmentTabsKeywords();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tabs_products, container, false);

        recyclerViewProducts=root.findViewById(R.id.recycler_products);
        txtvwSno = root.findViewById(R.id.txtvw_sno);
        txtvwProductname = root.findViewById(R.id.txtvw_productname);
        txtvwBrand = root.findViewById(R.id.txtvw_brand);
        txtvwPptfiles = root.findViewById(R.id.txtvw_pptfiles);
        linearLayout = root.findViewById(R.id.linearLayout);

        txtvwProductname.setText("Keywords");

        txtvwBrand.setVisibility(View.GONE);
        txtvwPptfiles.setVisibility(View.GONE);


        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)
                txtvwProductname.getLayoutParams();
        params.weight = 1.0f;
        txtvwProductname.setLayoutParams(params);


        product_list.add(new CommonObjects.Keywords(1, "ACRIDON",  true));
        product_list.add(new CommonObjects.Keywords(2, "Am-Pro",   false));
        product_list.add(new CommonObjects.Keywords(3, "AQUASAN",  true));
        product_list.add(new CommonObjects.Keywords(4, "ASSIST-PP",   false));
        product_list.add(new CommonObjects.Keywords(5, "B4",  true));
        product_list.add(new CommonObjects.Keywords(6, "BGN",   true));
        product_list.add(new CommonObjects.Keywords(7, "BOOMIN",  true));
        product_list.add(new CommonObjects.Keywords(8, "BOOMIN FORTE",   false));
        product_list.add(new CommonObjects.Keywords(9, "CaPoMag",  true));
        product_list.add(new CommonObjects.Keywords(10, "CHALLENGER",  true));


        Product_RecyclerList();


        return root;
    }

    private void setGridView(List<CommonObjects.Keywords> product_lists) {
        //Set Medical History RecylerView
        recyclerViewProducts.setLayoutManager(new LinearLayoutManager(getActivity()));
        final POSEIDON_RecyclerAdapter KDMCRecyclerAdapter = new POSEIDON_RecyclerAdapter(product_lists, R.layout.poseidon_products_griditem)
                .setRowItemView(new POSEIDON_RecyclerAdapter.AdapterView() {
                    @Override
                    public Object setAdapterView(ViewGroup parent, int viewType, int layoutId) {
                        return new FragmentTabsProducts.ProductsViewHolder(LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false));
                    }

                    @Override
                    public void onBindView(Object holder, final int position, Object data, final List<Object> dataList) {

                        FragmentTabsProducts.ProductsViewHolder myViewHolders = (FragmentTabsProducts.ProductsViewHolder) holder;
                        final CommonObjects.Keywords value = (CommonObjects.Keywords) data;

                        try {
                            myViewHolders.sno.setText(String.valueOf(position + 1));
                            myViewHolders.product_name.setText(value.getKeywords());

                            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)
                                    myViewHolders.product_name.getLayoutParams();
                            params.weight = 1.0f;
                            myViewHolders.product_name.setLayoutParams(params);

                            myViewHolders.brand_name.setVisibility(View.GONE);
                            myViewHolders.fileppt.setVisibility(View.GONE);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                });
        recyclerViewProducts.setAdapter(KDMCRecyclerAdapter);
    }


    private void setListView(List<CommonObjects.Keywords> product_lists) {
        //Set Medical History RecylerView



        recyclerViewProducts.setLayoutManager(new GridLayoutManager(getActivity(),2));
        final POSEIDON_RecyclerAdapter KDMCRecyclerAdapter = new POSEIDON_RecyclerAdapter(product_lists, R.layout.poseidon_keywords_rowitem)
                .setRowItemView(new POSEIDON_RecyclerAdapter.AdapterView() {
                    @Override
                    public Object setAdapterView(ViewGroup parent, int viewType, int layoutId) {
                        return new ProductsViewHolder(LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false));
                    }

                    @Override
                    public void onBindView(Object holder, final int position, Object data, final List<Object> dataList) {

                        ProductsViewHolder myViewHolders = (ProductsViewHolder) holder;
                        final CommonObjects.Keywords value = (CommonObjects.Keywords) data;

                        try {
                            myViewHolders.sno.setText(String.format("%s.", String.valueOf(position + 1)));
                            myViewHolders.product_name.setText(value.getKeywords());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                });
        recyclerViewProducts.setAdapter(KDMCRecyclerAdapter);
    }


    public static class ProductsViewHolder extends RecyclerView.ViewHolder {
        TextView product_name;
        TextView sno;
        ProductsViewHolder(View view) {
            super(view);
            product_name = view.findViewById(R.id.txtvw_productname);
            sno = view.findViewById(R.id.txtvw_sno);
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    void filterList(String value) {
        //Do searching
        if (!value.isEmpty()) {
               /*   List<CommonObjects.ProductInfo> result = product_list.stream()                // convert list to stream
                    .filter(line -> !line.getProductName().startsWith(value))     // we dont like mkyong
                    .collect(Collectors.toList());*/

            searchList = new ArrayList<>();
            for (CommonObjects.Keywords productInfo : product_list) {
                if (productInfo.getKeywords().toLowerCase().startsWith(value.toLowerCase())) {
                    searchList.add(productInfo);
                }
            }

            if (viewType == CommonObjects.ViewType.ListView) {
                setListView(searchList);
            } else {
                setGridView(searchList);
            }
        }else
        {
            if (viewType == CommonObjects.ViewType.ListView) {
                setListView(product_list);
            } else {
                setGridView(product_list);
            }
        }
    }

    public void Product_RecyclerList() {
        //Set Medical History RecylerView

        if (viewType== CommonObjects.ViewType.ListView) {
            setListView(product_list);
        }else
        {
            setGridView(product_list);
        }

    }

}
