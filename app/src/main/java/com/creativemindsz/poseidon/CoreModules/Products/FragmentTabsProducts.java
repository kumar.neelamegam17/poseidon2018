package com.creativemindsz.poseidon.CoreModules.Products;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.creativemindsz.poseidon.Adapters.CommonObjects;
import com.creativemindsz.poseidon.OtherUtils.POSEIDON_RecyclerAdapter;
import com.creativemindsz.poseidon.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentTabsProducts extends Fragment {


    RecyclerView recyclerViewProducts;
    List<CommonObjects.ProductInfo> product_list = new ArrayList();

    List<CommonObjects.ProductInfo> searchList=new ArrayList<>();

    LinearLayout linearlayout;
   CommonObjects.ViewType viewType= CommonObjects.ViewType.ListView;

    public FragmentTabsProducts() {
    }

    public static FragmentTabsProducts newInstance() {
        FragmentTabsProducts fragment = new FragmentTabsProducts();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tabs_products, container, false);

        recyclerViewProducts=root.findViewById(R.id.recycler_products);
        linearlayout=root.findViewById(R.id.linearLayout);

        List<CommonObjects.Images> list = new ArrayList<>();
        CommonObjects.Images img_list = new CommonObjects.Images();
        img_list.setImgName("img1");
        img_list.setImgFileLocation("/images/img1.jpg");
        list.add(img_list);

        product_list.add(new CommonObjects.ProductInfo(1, "ACRIDON", "POSEIDON BIOTECH", "16052018033651.pptx", "-", list, true));
        product_list.add(new CommonObjects.ProductInfo(2, "Am-Pro", "POSEIDON BIOTECH", "16052018033651.pptx", "-", list, true));
        product_list.add(new CommonObjects.ProductInfo(3, "AQUASAN", "POSEIDON BIOTECH", "16052028033651.pptx", "-", list, true));
        product_list.add(new CommonObjects.ProductInfo(4, "ASSIST-PP", "POSEIDON BIOTECH", "1605018033651.pptx", "-", list, true));
        product_list.add(new CommonObjects.ProductInfo(5, "B4", "POSEIDON BIOTECH", "16052018034651.pptx", "-", list, true));
        product_list.add(new CommonObjects.ProductInfo(6, "BGN", "POSEIDON BIOTECH", "16052018053651.pptx", "-", list, true));
        product_list.add(new CommonObjects.ProductInfo(7, "BOOMIN", "POSEIDON BIOTECH", "16052078033651.pptx", "-", list, true));
        product_list.add(new CommonObjects.ProductInfo(8, "BOOMIN FORTE", "POSEIDON BIOTECH", "5052018033651.pptx", "-", list, true));
        product_list.add(new CommonObjects.ProductInfo(9, "CaPoMag", "POSEIDON BIOTECH", "16052618033651.pptx", "-", list, true));
        product_list.add(new CommonObjects.ProductInfo(10, "CHALLENGER", "POSEIDON BIOTECH", "18052018033651.pptx", "-", list, true));


        Product_RecyclerList();

        return root;
    }

    public void Product_RecyclerList() {
        //Set Medical History RecylerView

        if (viewType== CommonObjects.ViewType.ListView) {
            setListView(product_list);
        }else
        {
            setGridView(product_list);
        }

    }


    private void setGridView(List<CommonObjects.ProductInfo> product_lists) {
        recyclerViewProducts.setLayoutManager(new LinearLayoutManager(getActivity()));
        final POSEIDON_RecyclerAdapter KDMCRecyclerAdapter = new POSEIDON_RecyclerAdapter(product_lists, R.layout.poseidon_products_griditem)
                .setRowItemView(new POSEIDON_RecyclerAdapter.AdapterView() {
                    @Override
                    public Object setAdapterView(ViewGroup parent, int viewType, int layoutId) {
                        return new ProductsViewHolder(LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false));
                    }

                    @Override
                    public void onBindView(Object holder, final int position, Object data, final List<Object> dataList) {

                        ProductsViewHolder myViewHolders = (ProductsViewHolder) holder;
                        final CommonObjects.ProductInfo value = (CommonObjects.ProductInfo) data;

                        try {
                            myViewHolders.sno.setText(String.valueOf(position + 1));
                            myViewHolders.product_name.setText(value.getProductName());
                            myViewHolders.brand_name.setText(value.getBrand());
                            myViewHolders.fileppt.setText(value.getFilePPT());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                });
        recyclerViewProducts.setAdapter(KDMCRecyclerAdapter);
    }



    private void setListView(List<CommonObjects.ProductInfo> product_lists) {
        recyclerViewProducts.setLayoutManager(new GridLayoutManager(getActivity(),2));
        final POSEIDON_RecyclerAdapter KDMCRecyclerAdapter = new POSEIDON_RecyclerAdapter(product_lists, R.layout.poseidon_products_rowitem)
                .setRowItemView(new POSEIDON_RecyclerAdapter.AdapterView() {
                    @Override
                    public Object setAdapterView(ViewGroup parent, int viewType, int layoutId) {
                        return new ProductsViewHolder(LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false));
                    }

                    @Override
                    public void onBindView(Object holder, final int position, Object data, final List<Object> dataList) {

                        ProductsViewHolder myViewHolders = (ProductsViewHolder) holder;
                        final CommonObjects.ProductInfo value = (CommonObjects.ProductInfo) data;

                        try {
                            myViewHolders.sno.setText(String.valueOf(position + 1));
                            myViewHolders.product_name.setText(value.getProductName());
                            myViewHolders.brand_name.setText(value.getBrand());
                            myViewHolders.fileppt.setText(value.getFilePPT());

                            myViewHolders.Parent.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    Intent passIntent = new Intent(getActivity(), ProductDetailedView.class);
                                    passIntent.putExtra("SNO", String.valueOf(position+1));
                                    passIntent.putExtra("PRODUCT_NAME", value.getProductName());
                                    passIntent.putExtra("BRAND_NAME", value.getBrand());
                                    passIntent.putExtra("FILES_PPT", value.getFilePPT());
                                    passIntent.putExtra("ISDEFAULT", value.getIsDefault());
                                    startActivity(passIntent);

                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                });
        recyclerViewProducts.setAdapter(KDMCRecyclerAdapter);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    void filterList(String value) {
        //Do searching
        if (!value.isEmpty()) {
    /*        List<CommonObjects.ProductInfo> result = product_list.stream()                // convert list to stream
                    .filter(line -> !line.getProductName().startsWith(value))     // we dont like mkyong
                    .collect(Collectors.toList());*/

            searchList = new ArrayList<>();
            for (CommonObjects.ProductInfo productInfo : product_list) {
                if (productInfo.getProductName().toLowerCase().startsWith(value.toLowerCase())) {
                    searchList.add(productInfo);
                }
            }

            if (viewType == CommonObjects.ViewType.ListView) {
                setListView(searchList);
            } else {
                setGridView(searchList);
            }
        }else
        {
            if (viewType == CommonObjects.ViewType.ListView) {
                setListView(product_list);
            } else {
                setGridView(product_list);
            }
        }
    }


    public static class ProductsViewHolder extends RecyclerView.ViewHolder {
        TextView product_name;
        TextView brand_name;
        TextView fileppt;
        TextView sno;
        LinearLayout Parent;

        public ProductsViewHolder(View view) {
            super(view);

            Parent = view.findViewById(R.id.parent_layout);
            product_name = view.findViewById(R.id.txtvw_productname);
            brand_name = view.findViewById(R.id.txtvw_brand);
            fileppt = view.findViewById(R.id.txtvw_pptfiles);
            sno = view.findViewById(R.id.txtvw_sno);
        }
    }


}
