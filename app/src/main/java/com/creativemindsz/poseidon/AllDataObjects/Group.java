package com.creativemindsz.poseidon.AllDataObjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Group implements Serializable {
    private int id;
    private String date;
    private String name;
    private String snippet;
    private String photo;
    private ArrayList<Friend> friends = new ArrayList<>();


    public Group(int id, String name, String snippet, String photo) {
        this.id = id;
        this.name = name;
        this.snippet = snippet;
        this.photo = photo;
    }


    public int getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public String getSnippet() {
        return snippet;
    }

    public String getPhoto() {
        return photo;
    }

    public List<Friend> getFriends() {
        return friends;
    }
    public String getMember() {
        if (friends.size() > 100) {
            return "100+ members";
        }
        return (friends.size() + 1) + " members";
    }
}
