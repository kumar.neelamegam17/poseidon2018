package com.creativemindsz.poseidon.Adapters;

import android.graphics.drawable.Drawable;

import java.util.List;

public class CommonObjects {




    public static class Image {

        public int image;
        public Drawable imageDrw;
        public String name;
        public String brief;
        public Integer counter = null;

    }



    public static enum ViewType
    {
        ListView,GrideView
    }



    public static class Images
    {
        String ImgName;
        String ImgFileLocation;

        public String getImgName() {
            return ImgName;
        }

        public void setImgName(String imgName) {
            ImgName = imgName;
        }

        public String getImgFileLocation() {
            return ImgFileLocation;
        }

        public void setImgFileLocation(String imgFileLocation) {
            ImgFileLocation = imgFileLocation;
        }
    }

    public static class ProductInfo
    {


        int Id;
        String ProductName;
        String Brand;
        String FilePPT;
        String Images;
        List<Images> Images_Path;
        boolean IsDefault;

        public ProductInfo(int id, String productName, String brand, String filePPT, String images,List<Images> images_Path, boolean isDefault) {
            Id = id;
            ProductName = productName;
            Brand = brand;
            FilePPT = filePPT;
            Images = images;
            Images_Path = images_Path;
            IsDefault = isDefault;
        }

        public boolean getIsDefault() {
            return IsDefault;
        }

        public void setDefault(boolean aDefault) {
            IsDefault = aDefault;
        }

        public void setImages_Path(List<Images> images_Path) {
            Images_Path = images_Path;
        }

        public List<Images> getImages_Path() {
            return Images_Path;
        }

        public void setId(int id) {
            Id = id;
        }

        public void setBrand(String brand) {
            Brand = brand;
        }

        public void setProductName(String productName) {
            ProductName = productName;
        }

        public void setFilePPT(String filePPT) {
            FilePPT = filePPT;
        }

        public void setImages(String images) {
            Images = images;
        }

        public int getId() {
            return Id;
        }

        public String getProductName() {
            return ProductName;
        }

        public String getBrand() {
            return Brand;
        }

        public String getFilePPT() {
            return FilePPT;
        }

        public String getImages() {
            return Images;
        }


    }


    public static class Keywords
    {
        int Id;
        String Keywords;
        boolean IsDefault;

        public Keywords(int id, String keywords, boolean b) {
            this.Id = id;
            this.Keywords = keywords;
            this.IsDefault = b;

        }

        public void setDefault(boolean aDefault) {
            IsDefault = aDefault;
        }

        public void setId(int id) {
            Id = id;
        }

        public void setKeywords(String keywords) {
            Keywords = keywords;
        }

        public boolean isDefault() {
            return IsDefault;
        }

        public int getId() {
            return Id;
        }

        public String getKeywords() {
            return Keywords;
        }


    }



    public static class KeywordsxApplication
    {
        int Id;
        String Keywords;
        String WaterApp;
        String FoodApp;

        public KeywordsxApplication(int id, String keywords, String waterapp, String foodapp) {
            this.Id = id;
            this.Keywords = keywords;
            this.WaterApp = waterapp;
            this.FoodApp = foodapp;
        }

        public void setId(int id) {
            Id = id;
        }

        public void setKeywords(String keywords) {
            Keywords = keywords;
        }

        public void setFoodApp(String foodApp) {
            FoodApp = foodApp;
        }

        public void setWaterApp(String waterApp) {
            WaterApp = waterApp;
        }

        public int getId() {
            return Id;
        }

        public String getKeywords() {
            return Keywords;
        }

        public String getFoodApp() {
            return FoodApp;
        }

        public String getWaterApp() {
            return WaterApp;
        }

    }







}//END
