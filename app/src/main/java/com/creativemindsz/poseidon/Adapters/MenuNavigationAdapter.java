package com.creativemindsz.poseidon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.creativemindsz.poseidon.R;

public class MenuNavigationAdapter extends BaseExpandableListAdapter {

    private final Context mContext;
    private final LayoutInflater mLayoutInflater;
    private String[] mGroups;
    private String[][] mChilds;
    String[] mIcons;



    public MenuNavigationAdapter(Context context, String[] group, String[][] child, String[] icons) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mGroups = group;
        mChilds = child;
        mIcons = icons;

    }

    @Override
    public int getGroupCount() {
        return mGroups.length;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mGroups[groupPosition];
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.row_menunavigation_header, parent, false);
        }

        TextView category_title = convertView.findViewById(R.id.category_title);
        category_title.setText(mGroups[groupPosition]);


        ImageView Imgvw_Icon = convertView.findViewById(R.id.imgvw_icon);
        Glide.with(convertView.getContext()).load(getImage(mIcons[groupPosition],convertView.getContext())).into(Imgvw_Icon);

        return convertView;
    }

    public int getImage(String imageName,Context ctx) {

        int drawableResourceId = ctx.getResources().getIdentifier(imageName, "drawable", ctx.getPackageName());

        return drawableResourceId;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mChilds[groupPosition].length;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mChilds[groupPosition][childPosition];
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.row_menunavigation, parent, false);
        }
        TextView menuText = convertView.findViewById(R.id.menuText);
        menuText.setText(mChilds[groupPosition][childPosition]);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
