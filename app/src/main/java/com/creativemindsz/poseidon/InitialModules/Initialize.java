package com.creativemindsz.poseidon.InitialModules;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.creativemindsz.poseidon.CoreModules.Constants;
import com.creativemindsz.poseidon.OtherUtils.Tools;
import com.creativemindsz.poseidon.R;

public class Initialize extends AppCompatActivity {



    private static final int MAX_STEP = 4;

    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private Button btn_got_it;
    LinearLayout ip_layout;
    private String title_array[] = {
            "WELCOME TO POSEIDON BIOTECH",
            "GET PRODUCT INFORMATIONS",
            "ENROLL FARMERS DETAILS",
            "GET NOTIFICATIONS & ALERTS",
    };
    private String description_array[] = {
            "An aqua & agro biotechnology company",
            "Get all product information by brand wise, keywords etc..",
            "Add new farmers and they details",
            "Stay notified about meetings & targets",
    };
    private int about_images_array[] = {
            R.drawable.only_poseidon_logo,
            R.drawable.ic_vector_dashboard_menu2_products,
            R.drawable.ic_vector_dashboard_menu2_farmers,
            R.drawable.ic_vector_dashboard_menu2_messages
    };
    private int color_array[] = {
            R.color.colorPrimary,
            R.color.green_500,
            R.color.purple_500,
            R.color.deep_purple_500
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poseidon_initialize);

        initComponent();

        Tools.setSystemBarTransparent(this);

        btn_got_it.setOnClickListener(v -> Constants.globalStartIntent(Initialize.this, Splash.class, null));
    }


    private void initComponent() {
        viewPager = findViewById(R.id.view_pager);
        btn_got_it = findViewById(R.id.btn_got_it);
        ip_layout = findViewById(R.id.ip_layout);
        // adding bottom dots
        bottomProgressDots(0);

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        btn_got_it.setVisibility(View.GONE);
        ip_layout.setVisibility(View.GONE);

    }

    private void bottomProgressDots(int current_index) {
        LinearLayout dotsLayout = findViewById(R.id.layoutDots);
        ImageView[] dots = new ImageView[MAX_STEP];

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new ImageView(this);
            int width_height = 15;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(width_height, width_height));
            params.setMargins(10, 10, 10, 10);
            dots[i].setLayoutParams(params);
            dots[i].setImageResource(R.drawable.shape_circle);
            dots[i].setColorFilter(getResources().getColor(R.color.overlay_dark_30), PorterDuff.Mode.SRC_IN);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0) {
            dots[current_index].setImageResource(R.drawable.shape_circle);
            dots[current_index].setColorFilter(getResources().getColor(R.color.grey_10), PorterDuff.Mode.SRC_IN);
        }
    }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(final int position) {
            bottomProgressDots(position);
            if (position == title_array.length - 1) {
                btn_got_it.setVisibility(View.VISIBLE);
                ip_layout.setVisibility(View.VISIBLE);
            } else {
                btn_got_it.setVisibility(View.GONE);
                ip_layout.setVisibility(View.GONE);
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(R.layout.item_stepper_wizard_color, container, false);
            ((TextView) view.findViewById(R.id.title)).setText(title_array[position]);
            ((TextView) view.findViewById(R.id.description)).setText(description_array[position]);
            ((ImageView) view.findViewById(R.id.image)).setImageResource(about_images_array[position]);
            view.findViewById(R.id.lyt_parent).setBackgroundColor(getResources().getColor(color_array[position]));
            container.addView(view);

            Tools.setSystemBarColor(Initialize.this,color_array[position]);

            return view;
        }

        @Override
        public int getCount() {
            return title_array.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }





}//END
