package com.creativemindsz.poseidon.InitialModules;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.creativemindsz.poseidon.CoreModules.Constants;
import com.creativemindsz.poseidon.CoreModules.Dashboard;
import com.creativemindsz.poseidon.R;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poseidon_login);

        findViewById(R.id.button_signin).setOnClickListener(v -> Constants.globalStartIntent(Login.this, Dashboard.class, null));

    }
}
