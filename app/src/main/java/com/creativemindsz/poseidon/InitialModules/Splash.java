package com.creativemindsz.poseidon.InitialModules;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.creativemindsz.poseidon.CoreModules.Constants;
import com.creativemindsz.poseidon.R;

public class Splash extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poseidon_splash);

        findViewById(R.id.imageView3).setOnClickListener(v -> Constants.globalStartIntent(Splash.this, Login.class, null));

    }


}
